+++
title = "About"
date = 2022-09-30T15:34:57+03:00
description = "About Section"
author = "Dimitrios Pappas"
+++

Hi and welcome to my personal website! My name is **Dimitrios Pappas** and I'm a Software Engineer. I created this website to showcase my work and connect with other professionals in my field. If you would like to get in touch with me, please follow the icons under my profile picture.

---
