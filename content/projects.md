+++
title = "Projects"
date = 2023-02-06T15:43:51+03:00
description = "Projects Section"
author = "Dimitrios Pappas"
+++


## Personal Projects

- [Simple Auto Clicker](https://github.com/DimitrisPa/Simple-Auto-Clicker) - A simple Auto-clicker GUI designed for incremental games like Cookie Clicker written in Python.

## Community Projects

- [Awesome Linux Software](https://github.com/luong-komorebi/Awesome-Linux-Software) (_Maintainer_) - A list of awesome Linux softwares.

---
